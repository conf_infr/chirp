#/bin/bash
apt install mosquitto mosquitto-clients redis-server redis-tools postgresql git
cd /
postgres psql -c «CREATE ROLE lora_ns WITH LOGIN PASSWORD ‘lora_ns’;»
postgres psql -c «CREATE DATABASE lora_ns WITH OWNER lora_ns;»
postgres psql -c «CREATE ROLE lora_as WITH LOGIN PASSWORD ‘lora_as’;»
postgres psql -c «CREATE DATABASE lora_as WITH OWNER lora_as;»
postgres psql lora_as -c «CREATE EXTENSION pg_trgm;»
postgres psql lora_as -c «CREATE EXTENSION hstore;»
cd /opt
wget https://artifacts.chirpstack.io/downloads/chirpstack-gateway-bridge/chirpstack-gateway-bridge_3.13.1_linux_amd64.deb
wget https://artifacts.chirpstack.io/downloads/chirpstack-network-server/chirpstack-network-server_3.15.3_linux_amd64.deb
wget https://artifacts.chirpstack.io/downloads/chirpstack-application-server/chirpstack-application-server_3.17.3_linux_amd64.deb
dpkg -i chirpstack-gateway-bridge*
dpkg -i chirpstack-network-server*
dpkg -i chirpstack-application-server*
git clone https://gitlab.com/conf_infr/chirp.git
cp chirpstack-application-server.toml /etc/chirpstack-application-server/chirpstack-application-server.toml
cp chirpstack-network-server.toml /etc/chirpstack-network-server/chirpstack-network-server.toml
cp chirpstack-gateway-bridge.toml /etc/chirpstack-gateway-bridge/chirpstack-gateway-bridge.toml
systemctl enable chirpstack-gateway-bridge
systemctl enable chirpstack-network-server
systemctl enable chirpstack-application-server
systemctl start chirpstack-gateway-bridge
systemctl start chirpstack-network-server
systemctl start chirpstack-application-server
